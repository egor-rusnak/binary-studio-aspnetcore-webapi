﻿using System.Text.Json.Serialization;

namespace CoolParking.ConsoleUI.Models
{
    public class TopUpViewModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("Sum")]
        public decimal Sum { get; set; }
    }
}
