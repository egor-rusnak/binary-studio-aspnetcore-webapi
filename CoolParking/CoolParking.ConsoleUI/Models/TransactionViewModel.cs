﻿using System;
using System.Text.Json.Serialization;

namespace CoolParking.ConsoleUI.Models
{
    public class TransactionViewModel
    {
        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")]
        public DateTimeOffset DateAndTime { get; set; }
        [JsonPropertyName("vehicleId")]
        public string VehicleId { get; set; }

        public override string ToString()
        {
            return $"{DateAndTime:G}: {Sum} money withdraw from vehicle with Id='{VehicleId}'.";
        }
    }
}
