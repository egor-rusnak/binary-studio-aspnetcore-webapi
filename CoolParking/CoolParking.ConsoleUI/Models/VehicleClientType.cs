﻿namespace CoolParking.ConsoleUI.Models
{
    public enum VehicleClientType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}
