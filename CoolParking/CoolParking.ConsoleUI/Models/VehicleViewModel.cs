﻿using System.Text.Json.Serialization;

namespace CoolParking.ConsoleUI.Models
{

    public class VehicleViewModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }
        [JsonPropertyName("vehicleType")]
        public VehicleClientType Type { get; set; }
    }
}
