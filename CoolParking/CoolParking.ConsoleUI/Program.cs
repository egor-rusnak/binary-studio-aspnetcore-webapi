﻿
using System;
using System.Linq;

namespace CoolParking.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            string apiUrl = args.ElementAtOrDefault(0);
            if (string.IsNullOrEmpty(apiUrl))
                apiUrl = "https://localhost:5001/api/";

            Console.WriteLine("Starting on " + apiUrl);
            try
            {
                var app = new ParkingApp(apiUrl);
                app.StartParking();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't continue! Error: " + ex.Message);
            }
        }
    }
}
