﻿using CoolParking.ConsoleUI.Extensions;
using CoolParking.ConsoleUI.Interfaces;
using CoolParking.ConsoleUI.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CoolParking.ConsoleUI.Services
{
    public class ParkingHttpService : IParkingService
    {
        private HttpClient _client;

        public ParkingHttpService(string url)
            => SetSource(url);

        public void SetSource(string source)
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            HttpClient client = new HttpClient(clientHandler);
            client.BaseAddress = new Uri(source);
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            _client?.Dispose();
            _client = client;
        }

        public void AddVehicle(VehicleViewModel vehicle)
            => _client.MakePostRequest("vehicles", vehicle);

        public decimal GetBalance()
            => _client.MakeGetRequest<decimal>("parking/balance");

        public int GetCapacity()
            => _client.MakeGetRequest<int>("parking/capacity");

        public int GetFreePlaces()
            => _client.MakeGetRequest<int>("parking/freeplaces");

        public TransactionViewModel[] GetLastParkingTransactions()
            => _client.MakeGetRequest<TransactionViewModel[]>("transactions/last");

        public IEnumerable<VehicleViewModel> GetVehicles()
            => _client.MakeGetRequest<List<VehicleViewModel>>("vehicles");

        public string GetPastTransactions()
            => _client.MakeGetRequest<string>("transactions/all");

        public void RemoveVehicle(string vehicleId)
            => _client.MakeDeleteRequest("vehicles/" + vehicleId);

        public void TopUpVehicle(string vehicleId, decimal sum)
            => _client.MakePutRequest<VehicleViewModel, TopUpViewModel>("transactions/topUpVehicle", new TopUpViewModel { Id = vehicleId, Sum = sum });

        public void Dispose()
            => _client?.Dispose();
    }
}
