﻿using CoolParking.ConsoleUI.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace CoolParking.ConsoleUI
{
    public static class Input
    {
        public static string ReadId(TextWriter wr, TextReader rd)
        {
            wr.Write("Enter vehicle id: ");
            return rd.ReadLine();
        }

        public static decimal ReadSum(TextWriter wr, TextReader rd)
        {
            wr.Write("Enter Sum: ");

            if (!decimal.TryParse(rd.ReadLine(), out decimal sum))
                throw new ArgumentException("Can't parse it to sum!");
            else
                return sum;
        }

        public static VehicleClientType ReadType(TextWriter wr, TextReader rd)
        {
            var typeNames = Enum.GetNames(typeof(VehicleClientType));
            wr.WriteLine("Choose a vehicle type: ");

            ShowMenu(typeNames, wr);

            if (!TryReadRangedValue(rd, wr, 1, typeNames.Length, out int answer))
                throw new ArgumentException("No item in menu like this!");

            if (Enum.TryParse(typeNames[answer - 1], out VehicleClientType result))
                return result;
            else
                throw new ArgumentException("Wrong input!");
        }

        public static void ShowMenu(IEnumerable<string> collecton, TextWriter wr)
        {
            int i = 1;
            foreach (string item in collecton)
                wr.WriteLine($"{i++}: {item}");
        }

        public static bool TryReadRangedValue(TextReader rd, TextWriter wr, int min, int max, out int readed)
        {
            if (int.TryParse(rd.ReadLine(), out readed) && readed >= min && readed <= max)
                return true;
            else
                return false;
        }
    }
}
