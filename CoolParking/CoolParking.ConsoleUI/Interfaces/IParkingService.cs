﻿using CoolParking.ConsoleUI.Models;
using System;
using System.Collections.Generic;

namespace CoolParking.ConsoleUI.Interfaces
{
    public interface IParkingService : IDisposable
    {
        void AddVehicle(VehicleViewModel vehicle);
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
        TransactionViewModel[] GetLastParkingTransactions();
        IEnumerable<VehicleViewModel> GetVehicles();
        string GetPastTransactions();
        void RemoveVehicle(string vehicleId);
        void TopUpVehicle(string vehicleId, decimal sum);

        void SetSource(string source);
    }
}