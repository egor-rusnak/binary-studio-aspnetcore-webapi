﻿using System.IO;

namespace CoolParking.ConsoleUI.Interfaces
{
    public interface IParkingCommand
    {
        string Description { get; init; }
        void Execute(IParkingService parking, TextReader rd, TextWriter wr);
    }
}
