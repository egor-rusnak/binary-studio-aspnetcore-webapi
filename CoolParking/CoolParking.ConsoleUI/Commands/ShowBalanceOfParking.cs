﻿using CoolParking.ConsoleUI.Interfaces;
using System.IO;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowBalanceOfParking : IParkingCommand
    {
        public string Description { get; init; } = "Show balance of parking";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
            => wr.WriteLine($"Parking balance is {parking.GetBalance()} money");
    }
}
