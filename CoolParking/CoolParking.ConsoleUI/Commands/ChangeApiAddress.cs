﻿using CoolParking.ConsoleUI.Interfaces;
using System;
using System.IO;

namespace CoolParking.ConsoleUI.Commands
{
    public class ChangeApiAddress : IParkingCommand
    {
        public string Description { get; init; } = "Change API address";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            wr.WriteLine("To start app custom api host, run shortcut with argument!");
            wr.WriteLine("Write a new host address (like https://localhost:5001/api/):");

            string input = rd.ReadLine();
            if (string.IsNullOrEmpty(input))
                throw new AggregateException("Bad HOST address");

            try
            {
                parking.SetSource(input);
            }
            catch (UriFormatException ex)
            {
                wr.WriteLine("Error: " + ex.Message);
            }
        }
    }
}
