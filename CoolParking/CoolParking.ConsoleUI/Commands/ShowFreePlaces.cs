﻿using CoolParking.ConsoleUI.Interfaces;
using System.IO;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowFreePlaces : IParkingCommand
    {
        public string Description { get; init; } = "Show Free Places";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
            => wr.WriteLine($"Free places: {parking.GetFreePlaces()} of {parking.GetCapacity()}");
    }
}
