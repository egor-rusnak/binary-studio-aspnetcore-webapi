﻿using CoolParking.ConsoleUI.Interfaces;
using System.IO;
using System.Linq;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowCurrentPeriodMoneyPaid : IParkingCommand
    {
        public string Description { get; init; } = "Show current period money paid";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            var sum = parking.GetLastParkingTransactions().Sum(e => e.Sum);
            wr.WriteLine($"Current period sum is {sum} money");
        }
    }
}
