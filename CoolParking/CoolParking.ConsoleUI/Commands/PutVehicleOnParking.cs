﻿using CoolParking.ConsoleUI.Interfaces;
using CoolParking.ConsoleUI.Models;
using System;
using System.IO;
using static CoolParking.ConsoleUI.Input;

namespace CoolParking.ConsoleUI.Commands
{
    public class PutVehicleOnParking : IParkingCommand
    {
        public string Description { get; init; } = "Put vehicle on Parking";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            try
            {
                parking.AddVehicle(new VehicleViewModel
                {
                    Id = ReadId(wr, rd),
                    Type = ReadType(wr, rd),
                    Balance = ReadSum(wr, rd)
                });

                wr.WriteLine("Vehicle was successfully added on parking!");
            }
            catch (ArgumentException e)
            {
                wr.WriteLine($"Error: {e.Message}");
            }
        }
    }
}
