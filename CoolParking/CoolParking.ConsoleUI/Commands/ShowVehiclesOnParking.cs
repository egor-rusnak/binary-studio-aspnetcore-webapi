﻿using CoolParking.ConsoleUI.Interfaces;
using System.IO;
using System.Linq;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowVehiclesOnParking : IParkingCommand
    {
        public string Description { get; init; } = "Show vehicles on parking";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            var vehiclesInString = parking.GetVehicles()
                .Select(v => $"Id: {v.Id}; Balance: {v.Balance} money; Type: {v.Type}");

            var result = string.Join("\n", vehiclesInString);

            if (!string.IsNullOrEmpty(result))
                wr.WriteLine($"Vehicles on parking:\n{result}");
            else
                wr.WriteLine("No vehicles on parking for now!");
        }
    }
}
