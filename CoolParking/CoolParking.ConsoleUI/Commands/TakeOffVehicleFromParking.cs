﻿using CoolParking.ConsoleUI.Interfaces;
using System;
using System.IO;
using System.Linq;
using static CoolParking.ConsoleUI.Input;

namespace CoolParking.ConsoleUI.Commands
{
    public class TakeOffVehicleFromParking : IParkingCommand
    {
        public string Description { get; init; } = "Take off Vehicle from Parking";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            if (!ShowVehiclesIfExists(parking, rd, wr))
                return;

            try
            {
                parking.RemoveVehicle(ReadId(wr, rd));
                wr.WriteLine("Vehicle was successfully removed!");
            }
            catch (ArgumentException e)
            {
                wr.WriteLine($"Error: {e.Message}");
            }
        }
        private bool ShowVehiclesIfExists(IParkingService parking, TextReader rd, TextWriter wr)
        {
            new ShowVehiclesOnParking().Execute(parking, rd, wr);

            if (parking.GetVehicles().Count() == 0)
                return false;
            else
                return true;
        }
    }
}
