﻿using CoolParking.ConsoleUI.Interfaces;
using CoolParking.ConsoleUI.Models;
using System.IO;

namespace CoolParking.ConsoleUI.Commands
{
    class ShowCurrentTransactions : IParkingCommand
    {
        public string Description { get; init; } = "Show current transactions";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            var transactions = parking.GetLastParkingTransactions();
            if (transactions.Length > 0)
                wr.WriteLine("Transactions:\n" + string.Join<TransactionViewModel>("\n", transactions));
            else
                wr.WriteLine("No transactions.");
        }
    }
}
