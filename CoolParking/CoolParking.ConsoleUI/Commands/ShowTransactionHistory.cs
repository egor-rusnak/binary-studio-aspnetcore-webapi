﻿using CoolParking.ConsoleUI.Interfaces;
using System;
using System.IO;

namespace CoolParking.ConsoleUI.Commands
{
    public class ShowTransactionHistory : IParkingCommand
    {
        public string Description { get; init; } = "Show transactions history";

        public void Execute(IParkingService parking, TextReader rd, TextWriter wr)
        {
            try
            {
                wr.Write($"Past transactions:\n{parking.GetPastTransactions()}");
            }
            catch (InvalidOperationException ex)
            {
                wr.WriteLine($"Error: {ex.Message}");
            }
        }
    }
}
