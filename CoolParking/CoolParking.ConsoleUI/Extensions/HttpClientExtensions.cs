﻿using System.Net.Http;
using System.Net.Http.Json;

namespace CoolParking.ConsoleUI.Extensions
{
    public static class HttpClientExtensions
    {
        public static T MakeGetRequest<T>(this HttpClient client, string url)
        {
            var response = client.GetAsync(url).Result;

            return ProcessJsonResponse<T>(response);
        }

        public static T MakePostRequest<T>(this HttpClient client, string url, T value)
        {
            var response = client.PostAsJsonAsync<T>(url, value).Result;

            return ProcessJsonResponse<T>(response);
        }

        public static void MakeDeleteRequest(this HttpClient client, string urlWithId)
        {
            var response = client.DeleteAsync(urlWithId).Result;

            ProcessStringResponce(response);
        }

        public static T MakePutRequest<T, U>(this HttpClient client, string url, U value)
        {
            var response = client.PutAsJsonAsync(url, value).Result;

            return ProcessJsonResponse<T>(response);
        }

        private static T ProcessJsonResponse<T>(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return response.Content.ReadFromJsonAsync<T>().Result;
            else
                throw new HttpRequestException(response.Content.ReadAsStringAsync().Result);
        }
        private static string ProcessStringResponce(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return response.Content.ReadAsStringAsync().Result;
            else
                throw new HttpRequestException(response.Content.ReadAsStringAsync().Result);
        }

    }
}
