﻿using CoolParking.ConsoleUI.Commands;
using CoolParking.ConsoleUI.Interfaces;
using CoolParking.ConsoleUI.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;

namespace CoolParking.ConsoleUI
{
    public class ParkingApp : IDisposable
    {
        private const string ExitCommand = "exit";

        private readonly TextWriter _writer = Console.Out;
        private readonly TextReader _reader = Console.In;

        private readonly string apiUrl = "https://localhost:5001/api/";

        private IReadOnlyList<IParkingCommand> commands { get; set; } = new List<IParkingCommand>(new IParkingCommand[]
        {
            new ShowVehiclesOnParking(),
            new ShowCurrentPeriodMoneyPaid(),
            new ShowTransactionHistory(),
            new ShowBalanceOfParking(),
            new ShowCurrentTransactions(),
            new ShowFreePlaces(),
            new PutVehicleOnParking(),
            new TakeOffVehicleFromParking(),
            new TopUpVehicleBalance(),
            new ChangeApiAddress()
        });

        private IParkingService _parking;

        public ParkingApp(string apiUrl)
        {
            this.apiUrl = apiUrl;
        }

        public ParkingApp(TextWriter wr, TextReader rd, string apiUrl) : this(apiUrl)
        {
            _writer = wr;
            _reader = rd;
        }

        public void StartParking()
        {
            ShowWelcomeMessage();
            InitializeParking();
            RunCommandLoop();
        }

        private void ShowWelcomeMessage()
        {
            _writer.WriteLine("Welcome to Cool Parking!");
            _writer.WriteLine("This app helps you to manage your parking with interface!");
        }

        private void InitializeParking()
        {
            _parking = BuildParkingWithServices();
        }

        private IParkingService BuildParkingWithServices()
        {
            _parking = new ParkingHttpService(apiUrl);

            return _parking;
        }

        private void RunCommandLoop()
        {
            while (true)
            {
                ShowMenuMessage();

                string input = _reader.ReadLine();
                if (int.TryParse(input, out int commandIndex) && commandIndex <= commands.Count && commandIndex > 0)
                {
                    try
                    {
                        ExecuteCommand(commands.ElementAt(commandIndex - 1));
                    }
                    catch (HttpRequestException ex)
                    {
                        _writer.WriteLine("Error: " + ex.Message);
                    }
                    catch (AggregateException ex)
                    {
                        _writer.WriteLine("Error: Connection Error, something wrong with connection to server! " + ex.Message);
                    }
                }
                else if (input == ExitCommand)
                {
                    Stop();
                    break;
                }
                else _writer.WriteLine("Input error, Try Again!");
            };
        }

        private void ShowMenuMessage()
        {
            Input.ShowMenu(commands.Select(c => c.Description), _writer);

            _writer.WriteLine($"{ExitCommand}: Exit");
            _writer.Write("Write a command >> ");
        }

        private void ExecuteCommand(IParkingCommand command)
        {
            _writer.WriteLine();
            command.Execute(_parking, _reader, _writer);
            _writer.WriteLine();
        }

        public void Stop()
        {
            ShowByeMessage();
            Dispose();
        }

        private void ShowByeMessage()
            => _writer.WriteLine("See you next time!");

        public void Dispose()
        {
            _writer.Dispose();
            _reader.Dispose();
        }
    }
}
