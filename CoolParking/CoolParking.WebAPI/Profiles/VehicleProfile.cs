﻿using AutoMapper;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Contracts;

namespace CoolParking.WebAPI.Profiles
{
    public class VehicleProfile : Profile
    {
        public VehicleProfile()
        {
            CreateMap<Vehicle, VehicleDto>();
            CreateMap<VehicleDto, Vehicle>();
        }
    }
}
