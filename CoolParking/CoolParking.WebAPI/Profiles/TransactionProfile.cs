﻿using AutoMapper;
using CoolParking.WebAPI.Contracts;

namespace CoolParking.WebAPI.Profiles
{
    public class TransactionProfile : Profile
    {
        public TransactionProfile()
        {
            CreateMap<TransactionInfo, TransactionDto>();
            CreateMap<TransactionDto, TransactionInfo>();
        }
    }
}
