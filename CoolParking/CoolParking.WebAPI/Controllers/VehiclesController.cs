﻿using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/{controller}")]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public VehiclesController(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VehicleDto>> List()
        {
            var vehicles = _parkingService.GetVehicles();
            return Ok(_mapper.Map<IEnumerable<VehicleDto>>(vehicles));
        }

        [HttpGet("{id}")]
        public ActionResult<VehicleDto> GetVehicle(string id)
        {
            if (!Vehicle.IsValidVehicleId(id))
                return BadRequest("This id is invalid!");

            var vehicle = _parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
                return NotFound("Vehicle with this id not found!");

            return Ok(_mapper.Map<VehicleDto>(vehicle));
        }

        [HttpPost]
        public ActionResult AddVehicle(VehicleDto vehicleModel)
        {
            try
            {
                var vehicle = _mapper.Map<Vehicle>(vehicleModel);

                _parkingService.AddVehicle(vehicle);
                return CreatedAtAction(nameof(GetVehicle), new { id = vehicle.Id }, vehicle);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult RemoveVehicle(string id)
        {
            if (!Vehicle.IsValidVehicleId(id))
                return BadRequest("Vehicle Id is invalid!");
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (InvalidOperationException ex)
            {
                return Forbid(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
