﻿using AutoMapper;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/{controller}")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        private readonly IMapper _mapper;

        public TransactionsController(IParkingService parkingService, IMapper mapper)
        {
            _parkingService = parkingService;
            _mapper = mapper;
        }

        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionDto>> GetLast()
        {
            return Ok(_mapper.Map<IEnumerable<TransactionDto>>(_parkingService.GetLastParkingTransactions()));
        }

        [HttpGet("all")]
        public ActionResult<string> GetAll()
        {
            try
            {
                var result = _parkingService.ReadFromLog();
                return Ok(result);
            }
            catch (InvalidOperationException)
            {
                return NotFound("Log not found!");
            }
        }

        [HttpPut("topUpVehicle")]
        public ActionResult<VehicleDto> TopUpVehicle(TopUpDto data)
        {
            if (!Vehicle.IsValidVehicleId(data.Id))
                return BadRequest("Id is not valid.");

            var vehicle = _parkingService.GetVehicles().FirstOrDefault(e => e.Id == data.Id);

            if (vehicle == null)
                return NotFound("There is no such car on parking");

            try
            {
                _parkingService.TopUpVehicle(data.Id, data.Sum);
                return Ok(_mapper.Map<VehicleDto>(vehicle));
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
