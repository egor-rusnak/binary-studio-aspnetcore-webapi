﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Contracts
{
    public class TopUpDto
    {
        [JsonPropertyName("id")]
        [Required]
        public string Id { get; set; }
        [JsonPropertyName("Sum")]
        [Required]
        public decimal Sum { get; set; }
    }
}
