﻿using CoolParking.BL.Models;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Contracts
{
    public class VehicleDto
    {
        [JsonPropertyName("id")]
        [Required]
        public string Id { get; set; }
        [JsonPropertyName("vehicleType")]
        [Required]
        public VehicleType Type { get; set; }
        [JsonPropertyName("balance")]
        [Required]
        public decimal Balance { get; set; }
    }
}
