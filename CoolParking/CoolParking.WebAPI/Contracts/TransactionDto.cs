﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.Contracts
{
    public class TransactionDto
    {
        [JsonPropertyName("vehicleId")]
        [Required]
        public string VehicleId { get; set; }
        [JsonPropertyName("sum")]
        [Required]
        public decimal Sum { get; set; }
        [JsonPropertyName("transactionDate")]
        [Required]
        public DateTimeOffset DateAndTime { get; set; }

    }
}
