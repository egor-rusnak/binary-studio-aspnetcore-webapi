using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Profiles;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Linq;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(e =>
            {
                e.Filters.Add(new ProducesAttribute("application/json"));
               
            }).AddJsonOptions(e=>
            {
                e.JsonSerializerOptions.PropertyNameCaseInsensitive = false;
            });

            services.AddAutoMapper(typeof(TransactionProfile), typeof(VehicleProfile));

            services.AddSingleton<ILogService, LogService>(
                e => new LogService(Directory.GetCurrentDirectory() + @"/Transactions.log"));

            services.AddTransient<ITimerService, TimerService>();

            services.AddSingleton<IParkingService, ParkingService>();

            services.AddLogging();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.Use(async (context, next) =>
            {
                await next.Invoke();
                var request = context.Request;
                Console.WriteLine("A request: " + request.ContentType + " " + request.Method + "\n" + string.Join("\n", request.RouteValues.Select(e => e.Key + " " + e.Value)));
                Console.WriteLine("Response: " + context.Response.StatusCode);
            });

            app.UseEndpoints(e => e.MapControllers());


            StartParking(app.ApplicationServices);
        }

        private void StartParking(IServiceProvider provider)
        {
            var service = provider.GetService<IParkingService>();
            if (service == null)
                throw new Exception("Please restart app!");

            Console.WriteLine("Parking started successfully!");
        }
    }
}
