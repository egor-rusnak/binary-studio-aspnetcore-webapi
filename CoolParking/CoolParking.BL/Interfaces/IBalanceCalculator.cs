﻿namespace CoolParking.BL.Interfaces
{
    public interface IBalanceCalculator
    {
        decimal Calculate(decimal balance, decimal withdraw, decimal penalty = 0);
    }
}
