﻿using Microsoft.VisualStudio.Services.Common;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public struct ParkingConfiguration
    {
        public decimal ParkingStartBalance { get; init; }
        public int ParkingCapacity { get; init; }
        public int PayPeriodInSec { get; init; }
        public int LogPeriodInSec { get; init; }
        public decimal Penalty { get; init; }
        public SerializableDictionary<VehicleType, decimal> ParkingRate { get; init; }

        public ParkingConfiguration(decimal startBalance,
            int capacity,
            int payPeriod,
            int logPeriod,
            decimal penalty,
            Dictionary<VehicleType, decimal> parkingRate)
        {
            ParkingStartBalance = startBalance;
            ParkingCapacity = capacity;
            PayPeriodInSec = payPeriod;
            LogPeriodInSec = logPeriod;
            Penalty = penalty;
            ParkingRate = new SerializableDictionary<VehicleType, decimal>().AddRange(parkingRate);
        }
        public static ParkingConfiguration GetDefaultConfiguration()
        {
            var config = new ParkingConfiguration(0, 10, 5, 60, 2.5m,
                new Dictionary<VehicleType, decimal>
                {
                    [VehicleType.Bus] = 3.5m,
                    [VehicleType.Motorcycle] = 1,
                    [VehicleType.PassengerCar] = 2,
                    [VehicleType.Truck] = 5
                }
            );
            return config;
        }
    }
}
