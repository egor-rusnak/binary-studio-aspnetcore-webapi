﻿using System;

public struct TransactionInfo
{
    public decimal Sum { get; set; }
    public decimal Balance { get; set; }
    public DateTimeOffset DateAndTime { get; set; }
    public string VehicleId { get; set; }

    public TransactionInfo(string vehicleId, decimal sum, decimal balance)
    {
        VehicleId = vehicleId;
        Sum = sum;
        DateAndTime = System.DateTime.Now;
        Balance = balance;
    }

    public override string ToString()
        => $"{DateAndTime:G}: {Sum} money withdraw from vehicle with Id='{VehicleId}'.";
}