﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; init; }
        public VehicleType VehicleType { get; init; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType type, decimal balance)
        {
            if (!IsValidVehicleId(id))
                throw new ArgumentException($"Bad vechicle Id: {id}");
            if (balance <= 0)
                throw new ArgumentException("Bad balance, should be more than 0");
            if (!Enum.IsDefined(type))
                throw new ArgumentException("Bad vehicle type");

            Id = id;
            VehicleType = type;
            Balance = balance;
        }

        public static bool IsValidVehicleId(string id)
        {
            var rg = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");

            if (!string.IsNullOrWhiteSpace(id) && rg.IsMatch(id))
                return true;
            else
                return false;
        }

        public static string GenerateRandomRegistrationPlateNumber()
            => GetRandomSymbols(2, 'A'..'Z') + "-" + GetRandomSymbols(4, '0'..'9') + "-" + GetRandomSymbols(2, 'A'..'Z');

        private static string GetRandomSymbols(int count, Range charRange)
        {
            var result = "";
            var rnd = new Random();

            for (int i = 0; i < count; i++)
                result += (char)rnd.Next(charRange.Start.Value, charRange.End.Value + 1);

            return result;
        }
    }
}