﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly string settingsFile = Directory.GetCurrentDirectory() + @"/Settings.xml";

        public static ParkingConfiguration GetParkingConfiguration()
        {
            try
            {
                return GetParkingConfigurationFromFile(settingsFile);
            }
            catch (IOException)
            {
                return ParkingConfiguration.GetDefaultConfiguration();
            }
        }

        private static void SaveSettings(ParkingConfiguration configToSave)
        {
            using (var fs = new FileStream(settingsFile, FileMode.Create))
            {
                var serializer = new XmlSerializer(typeof(ParkingConfiguration));
                serializer.Serialize(fs, configToSave);
            }
        }

        private static ParkingConfiguration GetParkingConfigurationFromFile(string path)
        {
            try
            {
                using (var fs = File.OpenRead(path))
                {
                    var serializer = new XmlSerializer(typeof(ParkingConfiguration));
                    return (ParkingConfiguration)serializer.Deserialize(fs);
                }
            }
            catch (Exception)
            {
                var config = ParkingConfiguration.GetDefaultConfiguration();
                SaveSettings(config);

                return config;
            }
        }
    }
}