﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _logTimer;
        private readonly ITimerService _withdrawTimer;
        private readonly ILogService _logService;
        private readonly IBalanceCalculator _balanceCalculator;

        private ParkingConfiguration Configuration;

        private readonly Parking _parking = Parking.Instance;
        private List<TransactionInfo> transactions = new();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _logService = logService;
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _balanceCalculator = new ParkingBalanceCalculator();

            SetUpTimersEvents();
            ConfigureParking();

            StartParking();
        }

        private void SetUpTimersEvents()
        {
            _withdrawTimer.Elapsed += WithdrawParkedVehicles;
            _logTimer.Elapsed += SaveTransactionsLog;
        }

        private void WithdrawParkedVehicles(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.Vehicles)
            {
                var resultSum = CalculateSumWithPenalty(GetWithdrawSumByVehicle(vehicle), vehicle.Balance);

                vehicle.Balance -= resultSum;
                _parking.Balance += resultSum;

                AddTransaction(resultSum, vehicle);
            }
        }

        private decimal GetWithdrawSumByVehicle(Vehicle vh)
            => Configuration.ParkingRate[vh.VehicleType];

        private decimal CalculateSumWithPenalty(decimal sum, decimal balance)
            => _balanceCalculator.Calculate(balance, sum, Configuration.Penalty);

        private void AddTransaction(decimal sum, Vehicle vh)
            => transactions.Add(new TransactionInfo(vh.Id, sum, vh.Balance));

        private void SaveTransactionsLog(object sender, ElapsedEventArgs e)
        {
            try
            {
                _logService.Write(string.Join("\n", transactions.ToList()));
                transactions.Clear();
            }
            catch (Exception)
            {
                //Not clear transaction and try nex time
            }
        }

        private void ConfigureParking()
        {
            UpdateConfiguration();
            SetPropertiesToParking();
        }

        private void UpdateConfiguration()
            => Configuration = Settings.GetParkingConfiguration();

        private void SetPropertiesToParking()
        {
            _parking.Capacity = Configuration.ParkingCapacity;
            _parking.Balance = Configuration.ParkingStartBalance;

            _withdrawTimer.Interval = Configuration.PayPeriodInSec * 1000;
            _logTimer.Interval = Configuration.LogPeriodInSec * 1000;
        }

        private void StartParking()
        {
            _withdrawTimer?.Start();
            _logTimer?.Start();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (sum <= 0)
                throw new ArgumentException("Sum must be more than 0");
            if (vehicle == null)
                throw new ArgumentException("There is no car with this id!");

            vehicle.Balance += sum;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() <= 0)
                throw new InvalidOperationException("No places for vehicle!");
            if (_parking.Vehicles.Any(v => v.Id == vehicle.Id))
                throw new ArgumentException("Vehicle with this Id is already in parking!");

            _parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Vehicle with this id is not exist!");
            if (vehicle.Balance < 0)
                throw new InvalidOperationException("This car has a debt! You can't remove it!");

            _parking.Vehicles.Remove(vehicle);
        }

        public decimal GetBalance() => _parking.Balance;

        public int GetCapacity() => _parking.Capacity;

        public int GetFreePlaces() => _parking.Capacity - _parking.Vehicles.Count;

        public TransactionInfo[] GetLastParkingTransactions() => transactions.ToArray();

        public ReadOnlyCollection<Vehicle> GetVehicles() => _parking.Vehicles.AsReadOnly();

        public string ReadFromLog() => _logService.Read();

        public void Dispose()
        {
            _parking.Vehicles.Clear();
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
        }
    }
}