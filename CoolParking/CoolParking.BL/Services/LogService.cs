﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; init; }
        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public string Read()
        {
            try
            {
                using (StreamReader stream = new StreamReader(File.OpenRead(LogPath)))
                {
                    var result = stream.ReadToEnd();
                    if (string.IsNullOrWhiteSpace(result))
                        throw new IOException();

                    return result;
                }
            }
            catch (IOException)
            {
                throw new InvalidOperationException("Log not found!");
            }
        }

        public void Write(string logInfo)
        {
            if (string.IsNullOrWhiteSpace(logInfo))
                return;
            try
            {
                using (StreamWriter stream = new StreamWriter(LogPath, true))
                {
                    stream.Write(logInfo + "\n");
                }
            }
            catch (IOException)
            {
                throw new InvalidOperationException("Can't write to log! ");
            }
        }
    }
}